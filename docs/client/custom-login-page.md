# Custom Login Page

## Table of Contents
1. [Custom Login Route](#custom-login-route)
2. [Troubleshooting](#troubleshooting)
3. [Support](#support)

## Custom Login Route
R-Auth allows the configuration of a custom login route. This section explains how to set up and manage custom login routes.

### Configuring Custom Login Route
1. Navigate to the **Clients** section in the R-Auth management console.
2. Select the client for which you want to configure a custom login route.
3. Enter the custom login route in the **Custom Login Route** field. This route should be relative to the authorization server.
4. Click **Save** to update the client settings.


## Troubleshooting
This section provides solutions to common issues faced while configuring custom login routes in R-Auth.

## Support
For additional help, contact R-Auth support- support@castlecraft.in
