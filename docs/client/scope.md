# Client Scope

## Table of Contents
1. [Scope Management](#scope-management)
2. [Troubleshooting](#troubleshooting)
3. [Support](#support)

## Scope Management
Scopes define the level of access that clients have. This section explains how to create and manage scopes in R-Auth.

### Creating Scopes
1. Navigate to the **Scopes** section in the R-Auth management console.
2. Click on **Create New Scope**.
3. Enter a unique name and description for the scope.
4. Click **Save** to create the scope.

### Editing Scopes
1. Navigate to the **Scopes** section.
2. Select the scope you wish to edit.
3. Update the necessary fields and click **Save**.

### Deleting Scopes
1. Navigate to the **Scopes** section.
2. Select the scope you wish to delete.
3. Click **Delete** and confirm the action.

## Troubleshooting
This section provides solutions to common issues faced while managing scopes in R-Auth.

## Support
For additional help, contact R-Auth support - support@castlecraft.in
